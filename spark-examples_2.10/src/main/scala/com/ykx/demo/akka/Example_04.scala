package org.ykx.demo.akka

import akka.actor.ActorLogging

/*
 *创建Actor,调用context.actorOf方法
 * 
 * 在Akka框架中，每个Akka应用程序都会有一个守卫Actor，名称为user，所有通过system.actorOf工厂方法创建的Actor都为user
 * 的子Actor，也是整个Akka程序的顶级Actor。
 * 
 */
object Example_04 {
  import akka.actor.Actor
  import akka.actor.ActorSystem
  import akka.actor.Props


  class FirstActor extends Actor with ActorLogging{
    //通过context.actorOf方法创建Actor
    val child = context.actorOf(Props[MyActor], name = "myChild")
    def receive = {
      case x => child ! x;log.info("received "+x)
    }

  }


  class MyActor extends Actor with ActorLogging{
    def receive = {
      case "test" => log.info("received test")
      case _      => log.info("received unknown message")
    }
  }
  
  class MyACT extends Actor with ActorLogging{
    def receive ={
      case "1" => log.info("This is one.")
      case "2" => log.info("This is two.")
      case "3" => log.info("This is three.")
      case _ => log.info("unkown.")
    }
  }

  def main(args: Array[String]): Unit = {
    val system = ActorSystem("MyActorSystem")
    val systemLog = system.log

    //创建FirstActor对象
    val myactor = system.actorOf(Props[FirstActor], name = "firstActor")
    
    val myactor1 = system.actorOf(Props[MyACT])

    systemLog.info(">>>>>>>>>>>准备向myactor发送消息")
    //向myactor发送消息
//    myactor ! "test"
//    myactor ! 123
//    Thread.sleep(5000)
    
    myactor1 ! "1"
    myactor1 ! "2"
    myactor1 ! 3
    
    
    //关闭ActorSystem，停止程序的运行
    system.shutdown()
  }
}