package org.ykx.demo.scala

object Test {

  //排序
  def sort(ls: List[Int]): List[Int] = {
    ls match {
      case Nil => Nil //列表是空列表，返回空列表  
      case base :: tail => { //不是空列表  
        val (left, right) = tail.partition(_ < base) //将列表尾部分成比首部元素小的部分和比首部元素大的部分    
        sort(left) ::: base :: sort(right) //组合成一个新的列表——sort(比首部小的部分)+首部+sort(比首部大的部分)  
      }
    }
  }
  
  def main(args: Array[String]): Unit = {

    /**
     *  :+ 表示在list后面追加一个元素产生了一个新的list， 原来list还是保持不变，
     *  可以通过以下方式对不可变集合list改变：  list = list :+ elem
     *
     * *
     */
        var lst: List[Int] = List()
        lst = lst :+ 1
        lst = lst :+ 2
        println("[List]: " + lst)

    //    val b: List[Int] = List(1, 342, 2, 44, 74, 21, 7, 68, 9, 110)
    //    println("[b]: " + sort(b))

    //    val str = None
    //    println("[str]： "+Some(str).getOrElse(123))  
    //    println("[str]： "+Some(str).orElse(Some(1)))  

   
    
  }
  
  
}