package com.ykx.demo.sql

import org.apache.spark.sql.SQLContext
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.Encoders

object DataSetTest {
  val conf = new SparkConf().setMaster ("local[*]").setAppName ("DataFrameTest")
  val sc = new SparkContext(conf)
  val sqlContext = new SQLContext(sc)
  
  def main(args: Array[String]): Unit = {
    case class Person(id: Long, name: String)
    
    import sqlContext.implicits._ 
    val df = sqlContext.createDataFrame(Seq((1000,"yakcy"),(1001,"bone"),(1002,"swift"))).toDF("id","name")
    val ds = sqlContext.createDataset(Seq((1000,"yakcy"),(1001,"bone"),(1002,"swift")))
    df.show()
    ds.show()
    
  }
}