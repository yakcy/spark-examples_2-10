package com.ykx.demo;

import java.awt.print.Printable;

import org.apache.hadoop.hbase.coprocessor.TestDoubleColumnInterpreter;

public class JavaTest {

	/**
	 * 
	 *@Describe: case字符串
	 *@param str
	 *@return
	 *@Author: yankexi
	 *@Date: 2018年1月26日 上午9:37:58
	 */
	private static String getSwithchStr(String str) {
		String s = "";
		switch (str) {
		case "a":
			s = "AB";break;
		case "c":
			s = "CD";break;
		case "e":
			s = "EF";break;
		default:
			s = "none";
		}

		return s;
	}
	
	/**
	 * 
	 *@Describe: case 数字
	 *@param num
	 *@return
	 *@Author: yankexi
	 *@Date: 2018年1月26日 上午9:38:33
	 */
	private static String getSwithchNum(int num) {
		String s = "";
		switch (num) {
		case 1:
			s = "AB";break;
		case 2:
			s = "CD";break;
		case 3:
			s = "EF";break;
		default:
			s = "none";
		}

		return s;
	}

	/**
	 * 
	 *@Describe: 反转指定长度的字符串
	 *@param str
	 *@param begin
	 *@param end
	 *@Author: yankexi
	 *@Date: 2018年1月26日 上午10:18:38
	 */
	private static void reverseString(StringBuilder str, int begin, int end) {
		if (begin > str.length() - 1) {
			begin = str.length() - 1;
		} else if (end > str.length() - 1) {
			end = str.length() - 1;
		}
		
		while (begin <= end) {
			char tmp = str.charAt(begin);
			str.setCharAt(begin, str.charAt(end));
			str.setCharAt(end, tmp);
			begin++;
			end--;
		}

		System.out.println(str);
	}
	
	
	public static void main(String[] args) {
//		System.out.println("[s]:" + getSwithchStr("a"));
//		System.out.println("[n]:" + getSwithchNum(2));
		
		 StringBuilder str = new StringBuilder("abcde");
		 reverseString(str, 1, 2);
		

	}
}
