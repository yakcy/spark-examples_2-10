package com.ykx.demo;

public class MainTest {

	/**
	 * 
	 *@Describe: 时间复杂度为O(n)的冒泡算法：递增排序
	 *@param arr
	 *@Author: yankexi
	 *@Date: 2018年1月29日 上午10:21:04
	 */
	public static void bubbleSort(int arr[]) {
		boolean didSwap;
		for (int i = 0, len = arr.length; i < len - 1; i++) {
			didSwap = false;
			for (int j = 0; j < len - i - 1; j++) {
				if (arr[j + 1] < arr[j]) {
					swap(arr, j, j + 1);
					didSwap = true;
				}
			}
			
			if (didSwap == false) {
				System.out.print("[冒泡排序-递增]: ");
				for (i = 0; i < arr.length; i++) {
					System.out.print(arr[i] + " ");
				}
				return;
			}
		}
	}
	
	/**
	 * 
	 *@Describe: 时间复杂度为O(n^2)的冒泡算法：递增排序
	 *@param arr
	 *@Author: yankexi
	 *@Date: 2018年1月29日 上午10:50:53
	 */
	public static void bubbleSort1(int arr[]) {
		for (int i = 0, len = arr.length; i < len - 1; i++) {
			for (int j = 0; j < len - i - 1; j++) {
				if (arr[j + 1] < arr[j]) {
					swap(arr, j, j + 1);
				}
			}
		}
		System.out.print("[冒泡排序-递增]: ");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}
	
	/**
	 * 
	 *@Describe: 交换值
	 *@param arr
	 *@param a
	 *@param b
	 *@Author: yankexi
	 *@Date: 2018年1月29日 上午10:20:48
	 */
	private static void swap(int arr[], int a, int b) {
		int tmp = arr[a];
		arr[a] = arr[b];
		arr[b] = tmp;
	}
	
	public static void main(String[] args) {
		int arr[]={14,52,1,5,6,22};
		//bubbleSort(arr);
		bubbleSort1(arr);
		
		System.out.println();
		System.out.println("[哈希值]："+"sfs".hashCode());
	}

}
