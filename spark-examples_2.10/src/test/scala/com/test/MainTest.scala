package com.test

import scala.collection.mutable.ListBuffer

object MainTest {
  /** 判断完美数规则：一个数的约数之和等于它本身 **/
  def sumOfFactors(number: Int): Int = {
    (1 /: (2 until number)) { (sum, i) => if (number % i == 0) sum + i else sum }
  }

  def isPerfect(num: Int): Boolean = {
    num == sumOfFactors(num)
  }

  def findPerfectNumbers(start: Int, end: Int) = {
    require(start > 1 && end >= start)
    var perfectNumbers = new ListBuffer[Int]
    val lst = new ListBuffer
    (start to end).foreach(num => if (isPerfect(num)) perfectNumbers += num)
    perfectNumbers.toList
  }

  def getID():String={
    println("My ID is 5003xxxxxxxxxxxxx.")
    "5003xxxxxxxxxxxxx"
  }
  
  
  def main(args: Array[String]): Unit = {
    //查找完美数
//    val list = findPerfectNumbers(2, 1000)
//    println("\nFound Perfect Numbers:" + list.mkString(","))
    
//    System.err.println("\n...........................")
//    println(Left("va sss").isRight)
    
     //lazy懒加载必须是不可变变量,当使用改变量时执行，且自加载仅一次
     lazy val id = getID();
     println(id) 
     println(id) 
      
      
  }
}